#!/usr/bin/env node
'use strict';

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

var _cli = require('cli');

var _cli2 = _interopRequireDefault(_cli);

var _inquirer = require('inquirer');

var _inquirer2 = _interopRequireDefault(_inquirer);

var _glob = require('glob');

var _glob2 = _interopRequireDefault(_glob);

var _lodash = require('lodash');

var _nodeCmd = require('node-cmd');

var _nodeCmd2 = _interopRequireDefault(_nodeCmd);

var _rimraf = require('rimraf');

var _rimraf2 = _interopRequireDefault(_rimraf);

var _lib = require('../lib');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var cwd = _path2.default.resolve('./');

var forceRemoval = false;
var createLockFile = false;

var options = _cli2.default.parse({
  lock: ['l', 'Create a package-lock.json first when not present', 'boolean', undefined],
  force: ['f', 'Force removal when no package-lock.json present', 'boolean', undefined]
});

function promptForLockFile() {
  _inquirer2.default.prompt([_lib.questions.promptLock]).then(function (answers) {
    var lock = answers.lock;

    createLockFile = lock === 'y';
    removeNM();
  });
}

function confirmForceRemovalWithoutLockFlag() {
  _inquirer2.default.prompt([_lib.questions.promptLockAfterForce, _lib.questions.confirmForce]).then(function (answers) {
    var lockAfterForce = answers.lockAfterForce,
        force = answers.force;

    createLockFile = lockAfterForce === 'y';
    forceRemoval = lockAfterForce === 'n' && force === 'y';
    removeNM();
  });
}

function decideOptions() {
  if (createLockFile) {
    forceRemoval = false;
    removeNM();
  } else if (forceRemoval) {
    confirmForceRemovalWithoutLockFlag();
  } else {
    promptForLockFile();
  }
}

function removeNM() {
  if (createLockFile) {
    lockThenRemove();
  } else if (forceRemoval) {
    noLockForceRemove();
  } else {
    removeNormal();
  }
}

function lockThenRemove() {
  (0, _glob2.default)('*/package.json', {
    cwd: cwd,
    realpath: true
  }, function (err, files) {
    if (files && files.length) {
      var filtered = (0, _lodash.filter)(files, function (f) {
        return !f.includes('node_modules');
      });
      var projects = filtered.map(function (f) {
        return f.split('/package.json')[0];
      });
      projects.forEach(function (projectPath) {
        var tryPath = projectPath + '/package-lock.json';
        (0, _glob2.default)(tryPath, {
          cwd: cwd,
          realpath: true
        }, function (err, list) {
          var filterPkgs = (0, _lodash.filter)(list, function (f) {
            return !f.includes('node_modules');
          });
          if (!filterPkgs.length) {
            _nodeCmd2.default.get('cd ' + projectPath + ' && npm shrinkwrap && mv ./npm-shrinkwrap.json ./package-lock.json', function (err, data) {
              removeNormal();
            });
          }
        });
      });
    }
  });
}

function noLockForceRemove() {
  (0, _glob2.default)('*/package.json', {
    cwd: cwd,
    realpath: true
  }, function (err, files) {
    if (files && files.length) {
      var filtered = (0, _lodash.filter)(files, function (f) {
        return !f.includes('node_modules');
      });
      var modulesDirs = filtered.map(function (f) {
        return f.split('/package.json')[0] + '/node_modules';
      });
      modulesDirs.forEach(function (dir) {
        (0, _rimraf2.default)(dir, function (e) {
          if (e) console.log(e);
        });
      });
    }
  });
}

function removeNormal() {
  (0, _glob2.default)('*/package-lock.json', {
    cwd: cwd,
    realpath: true
  }, function (err, files) {
    if (files && files.length) {
      var filtered = (0, _lodash.filter)(files, function (f) {
        return !f.includes('node_modules');
      });
      filtered.forEach(function (lockFilePath) {
        var projectDir = lockFilePath.split('/package-lock.json')[0];
        (0, _glob2.default)(projectDir + '/node_modules', {
          cwd: cwd,
          realpath: true
        }, function (err, dirs) {
          if (dirs && dirs.length) {
            dirs.forEach(function (d) {
              (0, _rimraf2.default)(d, function (e) {
                if (e) console.log(e);
              });
            });
          }
        });
      });
    }
  });
}

createLockFile = options.lock === true;
forceRemoval = options.force === true;

decideOptions();