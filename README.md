# moduless

rm -rf all your `node_modules` directories from cwd down.

if a directory doesn't contain a `package-lock.json`, moduless
will skip removal of `node_modules` in that directory.

if you want moduless to generate a `package-lock.json` each time it can't
find one and then continue removing `node_modules`, include `--lock` or `-l`.

alternatively, override the checks for `package-lock.json` files and force removal with `--force` or  `-f`.