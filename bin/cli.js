#!/usr/bin/env node

import path from 'path';
import cli from 'cli';
import inquirer from 'inquirer';
import glob from 'glob';
import { filter } from 'lodash';
import cmd from 'node-cmd';

import rimraf from 'rimraf';

import { questions } from '../lib';

const cwd = path.resolve('./');

let forceRemoval = false;
let createLockFile = false;

const options = cli.parse({
  lock: ['l', 'Create a package-lock.json first when not present', 'boolean', undefined],
  force: ['f', 'Force removal when no package-lock.json present', 'boolean', undefined],
});

function promptForLockFile() {
  inquirer.prompt([questions.promptLock]).then((answers) => {
    const { lock } = answers;
    createLockFile = lock === 'y';
    removeNM();
  });
}

function confirmForceRemovalWithoutLockFlag() {
  inquirer.prompt([questions.promptLockAfterForce, questions.confirmForce]).then((answers) => {
    const { lockAfterForce, force } = answers;
    createLockFile = lockAfterForce === 'y';
    forceRemoval = (lockAfterForce === 'n' && force === 'y');
    removeNM();
  });
}

function decideOptions() {
  if (createLockFile) {
    forceRemoval = false;
    removeNM();
  } else if (forceRemoval) {
    confirmForceRemovalWithoutLockFlag();
  } else {
    promptForLockFile();
  }
}

function removeNM() {
  if (createLockFile) {
    lockThenRemove();
  } else if (forceRemoval) {
    noLockForceRemove();
  } else {
    removeNormal();
  }
}

function lockThenRemove() {
  glob(`*/package.json`, {
    cwd,
    realpath: true,
  }, (err, files) => {
    if (files && files.length) {
      const filtered = filter(files, f => !f.includes('node_modules'));
      const projects = filtered.map((f) => f.split('/package.json')[0]);
      projects.forEach((projectPath) => {
        const tryPath = `${projectPath}/package-lock.json`;
        glob(tryPath, {
          cwd,
          realpath: true,
        }, (err, list) => {
          const filterPkgs = filter(list, f => !f.includes('node_modules'));
          if (!filterPkgs.length) {
            cmd.get(`cd ${projectPath} && npm shrinkwrap && mv ./npm-shrinkwrap.json ./package-lock.json`, (err, data) => {
              removeNormal();
            });
          }
        });
      });
    }
  });
}

function noLockForceRemove() {
  glob(`*/package.json`, {
    cwd,
    realpath: true,
  }, (err, files) => {
    if (files && files.length) {
      const filtered = filter(files, f => !f.includes('node_modules'));
      const modulesDirs = filtered.map((f) => f.split('/package.json')[0] + '/node_modules');
      modulesDirs.forEach((dir) => {
        rimraf(dir, (e) => {
          if (e) console.log(e);
        });
      });
    }
  });
}

function removeNormal() {
  glob(`*/package-lock.json`, {
    cwd,
    realpath: true,
  }, (err, files) => {
    if (files && files.length) {
      const filtered = filter(files, f => !f.includes('node_modules'));
      filtered.forEach((lockFilePath) => {
        const projectDir = lockFilePath.split('/package-lock.json')[0];
        glob(`${projectDir}/node_modules`, {
          cwd,
          realpath: true,
        }, (err, dirs) => {
          if (dirs && dirs.length) {
            dirs.forEach((d) => {
              rimraf(d, (e) => {
                if (e) console.log(e);
              });
            });
          }
        });
      });
    }
  });
}

createLockFile = (options.lock === true);
forceRemoval = (options.force === true);

decideOptions();
