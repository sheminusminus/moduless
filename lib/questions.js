export default {
  promptLock: {
    type: 'input',
    name: 'lock',
    message: `If I don't find a package-lock.json, create one? (Removal skipped otherwise). y/n`,
    default: 'n',
  },
  promptLockAfterForce: {
    type: 'input',
    name: 'lockAfterForce',
    message: `Create a package-lock.json if not found, before removing node_modules? (y/n)`,
    default: 'n',
  },
  confirmForce: {
    type: 'input',
    name: 'force',
    message: `Are you sure you want to remove node_modules if no package-lock.json found? y/n`,
    default: 'n',
    when: (answers) => {
      return (answers.lockAfterForce === 'n');
    },
  },
};
